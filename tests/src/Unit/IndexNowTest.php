<?php

namespace Drupal\Tests\index_now\Unit;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\index_now\Service\IndexNow;
use Drupal\index_now\Service\IndexNowKeyManagerInterface;
use GuzzleHttp\ClientInterface;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Tests the IndexNow service.
 *
 * @coversDefaultClass \Drupal\index_now\Service\IndexNow
 *
 * @group index_now
 */
class IndexNowTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The Index Now key manager.
   *
   * @var \Drupal\index_now\Service\IndexNowKeyManagerInterface
   */
  protected $indexNowKeyManager;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The logger channel factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerChannelFactory;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * The Index Now service.
   *
   * @var \Drupal\index_now\Service\IndexNowInterface
   */
  protected $indexNow;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->client = $this->prophesize(ClientInterface::class);
    $this->configFactory = $this->prophesize(ConfigFactoryInterface::class);
    $this->config = $this->prophesize(Config::class);
    $this->indexNowKeyManager = $this->prophesize(IndexNowKeyManagerInterface::class);
    $this->messenger = $this->prophesize(MessengerInterface::class);
    $this->loggerChannelFactory = $this->prophesize(LoggerChannelFactoryInterface::class);
    $this->logger = $this->prophesize(LoggerChannelInterface::class);

    $this->config
      ->get('default_engine')
      ->willReturn('bing');
    $this->configFactory
      ->getEditable('index_now.settings')
      ->willReturn($this->config->reveal());

    $this->loggerChannelFactory
      ->get('index_now')
      ->willReturn($this->logger->reveal());

    $this->indexNow = new IndexNow(
      $this->client->reveal(),
      $this->configFactory->reveal(),
      $this->indexNowKeyManager->reveal(),
      $this->messenger->reveal(),
      $this->loggerChannelFactory->reveal(),
    );
  }

  /**
   * @covers ::sendRequest
   */
  public function testNoApiKeySendRequest(): void {
    $this->indexNowKeyManager
      ->getKey()
      ->ShouldBeCalled()
      ->willReturn('');

    $page_url = Argument::type('string');
    $this->client->request('GET', $page_url)
      ->shouldNotBeCalled();

    $this->indexNow->sendRequest($page_url);
  }

}
