<?php

namespace Drupal\Tests\index_now\Unit;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Config;
use Drupal\Tests\UnitTestCase;
use Drupal\index_now\Service\IndexNowKeyManager;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Tests the IndexNowKeyManager service.
 *
 * @coversDefaultClass \Drupal\index_now\Service\IndexNowKeyManager
 *
 * @group index_now
 */
class IndexNowKeyManagerTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * The Index Now config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The UUID handler.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuid;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->configFactory = $this->prophesize(ConfigFactoryInterface::class);
    $this->config = $this->prophesize(Config::class);
    $this->uuid = $this->prophesize(UuidInterface::class);
  }

  /**
   * @covers ::getKey
   */
  public function testGetKeyIfNoApiKeySet(): void {
    $this->config->get('api_key')
      ->shouldBeCalled()
      ->willReturn(NULL);

    $this->configFactory->getEditable('index_now.settings')
      ->shouldBeCalled()
      ->willReturn($this->config->reveal());

    $indexNowKeyManager = new IndexNowKeyManager(
      $this->configFactory->reveal(),
      $this->uuid->reveal()
    );

    $key = $indexNowKeyManager->getKey();

    $this->assertTrue(is_string($key));
    $this->assertEmpty($key);
  }

  /**
   * @covers ::getKey
   */
  public function testGetKeyIfApiKeyEmpty(): void {
    $this->config->get('api_key')
      ->shouldBeCalled()
      ->willReturn('');

    $this->configFactory->getEditable('index_now.settings')
      ->shouldBeCalled()
      ->willReturn($this->config->reveal());

    $indexNowKeyManager = new IndexNowKeyManager(
      $this->configFactory->reveal(),
      $this->uuid->reveal()
    );

    $key = $indexNowKeyManager->getKey();

    $this->assertTrue(is_string($key));
    $this->assertEmpty($key);
  }

  /**
   * @covers ::getKey
   */
  public function testGetKeyIfApiKeyExists(): void {
    $fake_uuid = 'ac48ac60-f99b-4d6c-9b5c-cb45fdee1048';

    $this->config->get('api_key')
      ->shouldBeCalled()
      ->willReturn($fake_uuid);

    $this->configFactory->getEditable('index_now.settings')
      ->shouldBeCalled()
      ->willReturn($this->config->reveal());

    $indexNowKeyManager = new IndexNowKeyManager(
      $this->configFactory->reveal(),
      $this->uuid->reveal()
    );

    $key = $indexNowKeyManager->getKey();
    $this->assertEquals($fake_uuid, $key);
  }

}
