<?php

namespace Drupal\Tests\index_now\Unit;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\index_now\NodeOperations;
use Drupal\index_now\Service\IndexNowInterface;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;
use Drupal\user\UserStorageInterface;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Tests the NodeOperations class.
 *
 * @coversDefaultClass \Drupal\index_now\NodeOperations
 *
 * @group index_now
 */
class NodeOperationsTest extends UnitTestCase {

  use ProphecyTrait;

  const CONFIG_NAME = 'index_now.settings';

  const CONFIG_EXCLUDE_NAME = 'exclude_node_types';

  const CONFIG_EXCLUDE_EVENTS_NAME = 'exclude_node_events';

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Index Now service.
   *
   * @var \Drupal\index_now\Service\IndexNowInterface
   */
  protected $indexNow;

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * The Index Now node operations.
   *
   * @var \Drupal\index_now\NodeOperations
   */
  protected $nodeOperations;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->configFactory = $this->prophesize(ConfigFactoryInterface::class);
    $this->entityTypeManager = $this->prophesize(EntityTypeManagerInterface::class);
    $this->indexNow = $this->prophesize(IndexNowInterface::class);

    $this->userStorage = $this->prophesize(UserStorageInterface::class);
    $this->entityTypeManager
      ->getStorage('user')
      ->willReturn($this->userStorage->reveal());

    $this->nodeOperations = new NodeOperations(
      $this->configFactory->reveal(),
      $this->entityTypeManager->reveal(),
      $this->indexNow->reveal()
    );
  }

  /**
   * @covers ::isIndexable
   */
  public function testUserCantAccessContent(): void {
    $node = $this->prophesize(Node::class);

    $user_prophecy = $this->prophesize(User::class);
    $this->userStorage->load(Argument::any())
      ->ShouldBeCalled()
      ->willReturn($user_prophecy->reveal());

    $node->access('view', $user_prophecy)
      ->ShouldBeCalled()
      ->willReturn(FALSE);

    $this->configFactory
      ->getEditable(self::CONFIG_NAME)
      ->ShouldNotBeCalled();

    $this->assertTrue(!$this->nodeOperations->isIndexable($node->reveal(), 'foo'));
  }

  /**
   * @covers ::isIndexable
   */
  public function testEventIsExcluded(): void {
    $node = $this->prophesize(Node::class);

    $user_prophecy = $this->prophesize(User::class);
    $this->userStorage->load(Argument::any())
      ->ShouldBeCalled()
      ->willReturn($user_prophecy->reveal());

    $node->access('view', $user_prophecy)
      ->ShouldBeCalled()
      ->willReturn(TRUE);

    $config = $this->prophesize(Config::class);
    $config
      ->get(self::CONFIG_EXCLUDE_EVENTS_NAME)
      ->ShouldBeCalled()
      ->willReturn([
        'foo' => 'foo',
        'bar' => 0,
      ]);
    $config
      ->get(self::CONFIG_EXCLUDE_NAME)
      ->ShouldNotBeCalled();

    $this->configFactory
      ->getEditable(self::CONFIG_NAME)
      ->ShouldBeCalled()
      ->willReturn($config->reveal());

    $this->assertTrue(!$this->nodeOperations->isIndexable($node->reveal(), 'foo'));
  }

  /**
   * @covers ::isIndexable
   */
  public function testNodeTypeIsExcluded(): void {
    $node = $this->prophesize(Node::class);

    $user_prophecy = $this->prophesize(User::class);
    $this->userStorage->load(Argument::any())
      ->ShouldBeCalled()
      ->willReturn($user_prophecy->reveal());

    $node->access('view', $user_prophecy)
      ->ShouldBeCalled()
      ->willReturn(TRUE);

    $config = $this->prophesize(Config::class);
    $config
      ->get(self::CONFIG_EXCLUDE_EVENTS_NAME)
      ->ShouldBeCalled()
      ->willReturn([
        'foo' => 0,
        'bar' => 0,
      ]);
    $config
      ->get(self::CONFIG_EXCLUDE_NAME)
      ->ShouldBeCalled()
      ->willReturn(['foobar']);

    $this->configFactory
      ->getEditable(self::CONFIG_NAME)
      ->ShouldBeCalled()
      ->willReturn($config->reveal());

    $node->getType()
      ->ShouldBeCalled()
      ->willReturn('foobar');

    $this->assertTrue(!$this->nodeOperations->isIndexable($node->reveal(), 'foo'));
  }

  /**
   * @covers ::isIndexable
   */
  public function testNodeIsIndexable(): void {
    $node = $this->prophesize(Node::class);

    $user_prophecy = $this->prophesize(User::class);
    $this->userStorage->load(Argument::any())
      ->ShouldBeCalled()
      ->willReturn($user_prophecy->reveal());

    $node->access('view', $user_prophecy)
      ->ShouldBeCalled()
      ->willReturn(TRUE);

    $config = $this->prophesize(Config::class);
    $config
      ->get(self::CONFIG_EXCLUDE_EVENTS_NAME)
      ->ShouldBeCalled()
      ->willReturn([
        'foo' => 0,
        'bar' => 0,
      ]);
    $config
      ->get(self::CONFIG_EXCLUDE_NAME)
      ->ShouldBeCalled()
      ->willReturn(['barfoo']);
    $this->configFactory
      ->getEditable(self::CONFIG_NAME)
      ->ShouldBeCalled()
      ->willReturn($config->reveal());

    $node->getType()
      ->ShouldBeCalled()
      ->willReturn('foobar');

    $this->assertTrue($this->nodeOperations->isIndexable($node->reveal(), 'foo'));
  }

}
