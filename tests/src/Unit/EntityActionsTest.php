<?php

namespace Drupal\Tests\index_now\Unit;

use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\index_now\Hook\EntityActions;
use Drupal\index_now\NodeOperations;
use Drupal\index_now\PathAliasOperations;
use Drupal\index_now\TermOperations;
use Drupal\node\Entity\Node;
use Drupal\path_alias\Entity\PathAlias;
use Drupal\taxonomy\Entity\Term;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Tests the EntityActions class.
 *
 * @coversDefaultClass \Drupal\index_now\Hook\EntityActions
 *
 * @group index_now
 */
class EntityActionsTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * The class resolver.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected $classResolver;

  /**
   * The index_now node operations.
   *
   * @var Drupal\index_now\NodeOperations
   */
  protected $nodeOperations;

  /**
   * The index_now term operations.
   *
   * @var Drupal\index_now\TermOperations
   */
  protected $termOperations;

  /**
   * The index_now path alias operations.
   *
   * @var Drupal\index_now\PathAliasOperations
   */
  protected $pathAliasOperations;

  /**
   * The entity actions hooks class.
   *
   * @var \Drupal\index_now\Hook\EntityActions
   */
  protected $entityActions;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->classResolver = $this->prophesize(ClassResolverInterface::class);
    $this->nodeOperations = $this->prophesize(NodeOperations::class);
    $this->termOperations = $this->prophesize(TermOperations::class);
    $this->pathAliasOperations = $this->prophesize(PathAliasOperations::class);

    $this->entityActions = new EntityActions(
      $this->classResolver->reveal()
    );
  }

  /**
   * @covers ::pathAliasInsert
   */
  public function testPathAliasInsert(): void {
    $this->classResolver->getInstanceFromDefinition(pathAliasOperations::class)
      ->shouldBeCalled()
      ->willReturn($this->pathAliasOperations);
    $this->pathAliasOperations->pingIndexNow(Argument::any())
      ->shouldBeCalled();
    $alias = $this->prophesize(PathAlias::class);

    $this->entityActions->pathAliasInsert($alias->reveal());
  }

  /**
   * @covers ::nodeUpdate
   */
  public function testNodeUpdate(): void {
    $this->classResolver->getInstanceFromDefinition(NodeOperations::class)
      ->shouldBeCalled()
      ->willReturn($this->nodeOperations);
    $this->nodeOperations->pingIndexNow(Argument::any(), 'update')
      ->shouldBeCalled();
    $node = $this->prophesize(Node::class);

    $this->entityActions->nodeUpdate($node->reveal());
  }

  /**
   * @covers ::nodeDelete
   */
  public function testNodeDelete(): void {
    $this->classResolver->getInstanceFromDefinition(NodeOperations::class)
      ->shouldBeCalled()
      ->willReturn($this->nodeOperations);
    $this->nodeOperations->pingIndexNow(Argument::any(), 'delete')
      ->shouldBeCalled();
    $node = $this->prophesize(Node::class);

    $this->entityActions->nodeDelete($node->reveal());
  }

  /**
   * @covers ::taxonomyTermUpdate
   */
  public function testTaxonomyTermUpdate(): void {
    $this->classResolver->getInstanceFromDefinition(TermOperations::class)
      ->shouldBeCalled()
      ->willReturn($this->termOperations);
    $this->termOperations->pingIndexNow(Argument::any(), 'update')
      ->shouldBeCalled();
    $term = $this->prophesize(Term::class);

    $this->entityActions->taxonomyTermUpdate($term->reveal());
  }

  /**
   * @covers ::taxonomyTermDelete
   */
  public function testTaxonomyTermDelete(): void {
    $this->classResolver->getInstanceFromDefinition(TermOperations::class)
      ->shouldBeCalled()
      ->willReturn($this->termOperations);
    $this->termOperations->pingIndexNow(Argument::any(), 'delete')
      ->shouldBeCalled();
    $term = $this->prophesize(Term::class);

    $this->entityActions->taxonomyTermDelete($term->reveal());
  }

}
