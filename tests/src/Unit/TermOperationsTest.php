<?php

namespace Drupal\Tests\index_now\Unit;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\index_now\Service\IndexNowInterface;
use Drupal\index_now\TermOperations;
use Drupal\taxonomy\Entity\Term;
use Drupal\user\Entity\User;
use Drupal\user\UserStorageInterface;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Tests the TermOperations class.
 *
 * @coversDefaultClass \Drupal\index_now\TermOperations
 *
 * @group index_now
 */
class TermOperationsTest extends UnitTestCase {

  use ProphecyTrait;

  const CONFIG_NAME = 'index_now.settings';

  const CONFIG_EXCLUDE_NAME = 'exclude_vocabularies';

  const CONFIG_EXCLUDE_EVENTS_NAME = 'exclude_taxonomy_events';

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Index Now service.
   *
   * @var \Drupal\index_now\Service\IndexNowInterface
   */
  protected $indexNow;

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * The Index Now term operations.
   *
   * @var \Drupal\index_now\TermOperations
   */
  protected $termOperations;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->configFactory = $this->prophesize(ConfigFactoryInterface::class);
    $this->entityTypeManager = $this->prophesize(EntityTypeManagerInterface::class);
    $this->indexNow = $this->prophesize(IndexNowInterface::class);

    $this->userStorage = $this->prophesize(UserStorageInterface::class);
    $this->entityTypeManager
      ->getStorage('user')
      ->willReturn($this->userStorage->reveal());

    $this->termOperations = new TermOperations(
      $this->configFactory->reveal(),
      $this->entityTypeManager->reveal(),
      $this->indexNow->reveal()
    );
  }

  /**
   * @covers ::isIndexable
   */
  public function testUserCantAccessContent(): void {
    $term = $this->prophesize(Term::class);

    $user_prophecy = $this->prophesize(User::class);
    $this->userStorage->load(Argument::any())
      ->ShouldBeCalled()
      ->willReturn($user_prophecy->reveal());

    $term->access('view', $user_prophecy)
      ->ShouldBeCalled()
      ->willReturn(FALSE);

    $this->configFactory
      ->getEditable(self::CONFIG_NAME)
      ->ShouldNotBeCalled();

    $this->assertTrue(!$this->termOperations->isIndexable($term->reveal(), 'foo'));
  }

  /**
   * @covers ::isIndexable
   */
  public function testEventIsExcluded(): void {
    $term = $this->prophesize(Term::class);

    $user_prophecy = $this->prophesize(User::class);
    $this->userStorage->load(Argument::any())
      ->ShouldBeCalled()
      ->willReturn($user_prophecy->reveal());

    $term->access('view', $user_prophecy)
      ->ShouldBeCalled()
      ->willReturn(TRUE);

    $config = $this->prophesize(Config::class);
    $config
      ->get(self::CONFIG_EXCLUDE_EVENTS_NAME)
      ->ShouldBeCalled()
      ->willReturn([
        'foo' => 'foo',
        'bar' => 0,
      ]);
    $config
      ->get(self::CONFIG_EXCLUDE_NAME)
      ->ShouldNotBeCalled();

    $this->configFactory
      ->getEditable(self::CONFIG_NAME)
      ->ShouldBeCalled()
      ->willReturn($config->reveal());

    $this->assertTrue(!$this->termOperations->isIndexable($term->reveal(), 'foo'));
  }

  /**
   * @covers ::isIndexable
   */
  public function testVocabularyIsExcluded(): void {
    $term = $this->prophesize(Term::class);

    $user_prophecy = $this->prophesize(User::class);
    $this->userStorage->load(Argument::any())
      ->ShouldBeCalled()
      ->willReturn($user_prophecy->reveal());

    $term->access('view', $user_prophecy)
      ->ShouldBeCalled()
      ->willReturn(TRUE);

    $config = $this->prophesize(Config::class);
    $config
      ->get(self::CONFIG_EXCLUDE_EVENTS_NAME)
      ->ShouldBeCalled()
      ->willReturn([
        'foo' => 0,
        'bar' => 0,
      ]);
    $config
      ->get(self::CONFIG_EXCLUDE_NAME)
      ->ShouldBeCalled()
      ->willReturn(['foobar']);

    $this->configFactory
      ->getEditable(self::CONFIG_NAME)
      ->ShouldBeCalled()
      ->willReturn($config->reveal());

    $term->bundle()
      ->ShouldBeCalled()
      ->willReturn('foobar');

    $this->assertTrue(!$this->termOperations->isIndexable($term->reveal(), 'foo'));
  }

  /**
   * @covers ::isIndexable
   */
  public function testTermIsIndexable(): void {
    $term = $this->prophesize(Term::class);

    $user_prophecy = $this->prophesize(User::class);
    $this->userStorage->load(Argument::any())
      ->ShouldBeCalled()
      ->willReturn($user_prophecy->reveal());

    $term->access('view', $user_prophecy)
      ->ShouldBeCalled()
      ->willReturn(TRUE);

    $config = $this->prophesize(Config::class);
    $config
      ->get(self::CONFIG_EXCLUDE_EVENTS_NAME)
      ->ShouldBeCalled()
      ->willReturn([
        'foo' => 0,
        'bar' => 0,
      ]);
    $config
      ->get(self::CONFIG_EXCLUDE_NAME)
      ->ShouldBeCalled()
      ->willReturn(['barfoo']);
    $this->configFactory
      ->getEditable(self::CONFIG_NAME)
      ->ShouldBeCalled()
      ->willReturn($config->reveal());

    $term->bundle()
      ->ShouldBeCalled()
      ->willReturn('foobar');

    $this->assertTrue($this->termOperations->isIndexable($term->reveal(), 'foo'));
  }

}
