<?php

namespace Drupal\Tests\index_now\Kernel;

use Drupal\Component\Uuid\Uuid;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the IndexNowKeyManager service.
 *
 * @coversDefaultClass \Drupal\index_now\Service\IndexNowKeyManager
 *
 * @group index_now
 */
class IndexNowKeyManagerTest extends KernelTestBase {

  /**
   * The modules to load to run the test.
   *
   * @var array
   */
  protected static $modules = [
    'index_now',
  ];

  /**
   * The Index Now key manager.
   *
   * @var \Drupal\index_now\Service\IndexNowKeyManagerInterface
   */
  protected $indexNowKeyManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['index_now']);
    $this->indexNowKeyManager = $this->container->get('index_now.apikey.manager');
  }

  /**
   * @covers ::getKey
   */
  public function testGetKeyIfNoApiKeySet(): void {
    $result = $this->indexNowKeyManager->getKey();

    $this->assertTrue(is_string($result));
    $this->assertEmpty($result);
  }

  /**
   * @covers ::generateKey
   * @covers ::getKey
   */
  public function testGetKeyIfApiKeyExists(): void {
    $this->indexNowKeyManager->generateKey();
    $key = $this->indexNowKeyManager->getKey();

    $this->assertTrue(is_string($key));
    $this->assertNotEmpty($key);
    // Next assertion might be removed - any string value would be a valid key.
    $this->assertTrue(Uuid::isValid($key), $key . ' is not a valid uuid.');
  }

}
