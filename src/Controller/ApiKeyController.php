<?php

namespace Drupal\index_now\Controller;

use Drupal\Core\Cache\CacheableResponse;
use Drupal\Core\Controller\ControllerBase;
use Drupal\index_now\Service\IndexNowKeyManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * API key controller.
 */
class ApiKeyController extends ControllerBase {

  /**
   * API key controller constructor.
   *
   * @param \Drupal\index_now\Service\IndexNowKeyManagerInterface $indexNowKeyManager
   *   The Index Now key manager.
   */
  public function __construct(protected IndexNowKeyManagerInterface $indexNowKeyManager) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('index_now.apikey.manager')
    );
  }

  /**
   * Render the API key.
   *
   * @param string $api_key
   *   A string representing the api key.
   *
   * @return \Drupal\Core\Cache\CacheableResponse
   *   A cacheable response.
   */
  public function build(string $api_key): CacheableResponse {
    $state_api_key = $this->indexNowKeyManager->getKey();

    if (empty($state_api_key) || $state_api_key !== $api_key) {
      throw new NotFoundHttpException();
    }

    $response = new CacheableResponse($api_key, Response::HTTP_OK, ['content-type' => 'text/plain']);
    $metadata = $response->getCacheableMetadata();
    $metadata->addCacheTags($this->indexNowKeyManager->getCacheTags());

    return $response;
  }

}
