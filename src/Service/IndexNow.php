<?php

namespace Drupal\index_now\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use GuzzleHttp\ClientInterface;

/**
 * Index now class.
 */
class IndexNow implements IndexNowInterface {

  use StringTranslationTrait;

  /**
   * The Index now default engine.
   *
   * @var string
   */
  protected $defaultEngine;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * IndexNow constructor.
   *
   * @param \GuzzleHttp\ClientInterface $httpClient
   *   The HTTP client service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\index_now\Service\IndexNowKeyManagerInterface $indexNowKeyManager
   *   The Index Now key manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerChannelFactory
   *   The logger service.
   */
  public function __construct(
    protected ClientInterface $httpClient,
    protected ConfigFactoryInterface $configFactory,
    protected IndexNowKeyManagerInterface $indexNowKeyManager,
    protected MessengerInterface $messenger,
    protected LoggerChannelFactoryInterface $loggerChannelFactory,
  ) {
    $this->defaultEngine = $configFactory
      ->getEditable('index_now.settings')
      ->get('default_engine') ?? 'bing';
    $this->logger = $loggerChannelFactory->get('index_now');
  }

  /**
   * {@inheritdoc}
   */
  public function sendRequest(string $page_url): void {
    $api_key = $this->indexNowKeyManager->getKey();
    if (empty($api_key)) {
      return;
    }

    try {
      $url = $this->buildUrl($page_url, $api_key);
      $url = urldecode($url->toString());
      $request = $this->httpClient->request('GET', $url);
      if (!in_array($request->getStatusCode(), [200, 202])) {
        $this->messenger->addWarning($this->t("Index Now returned an error: %error", ['%error' => $request->getReasonPhrase()]));
        $this->logger->warning("Index Now returned an error: %error", ['%error' => $request->getReasonPhrase()]);
        return;
      }
      $this->logger->info("%page_url has been indexed now.", ['%page_url' => $page_url]);
    }
    catch (\Exception $e) {
      $this->messenger->addWarning($this->t("Index Now returned an error: %error", ['%error' => $e->getMessage()]));
      $this->logger->warning("Index Now returned an error: %error", ['%error' => $e->getMessage()]);
    }
  }

  /**
   * Build the search engine URL we want to hit to index now.
   *
   * @param string $page_url
   *   The page we want to index now on the search engines.
   * @param string $api_key
   *   The API key used to prove ownership of the host.
   *
   * @return \Drupal\Core\Url
   *   The search engine URL we want to hit to index now.
   */
  protected function buildUrl(string $page_url, string $api_key): Url {
    $engine_url = self::ENDPOINTS[$this->defaultEngine] ?? '';

    if (empty($engine_url)) {
      throw new \Exception('invalid Index Now engine URL.');
    }

    $endpoint = $engine_url . "/indexnow";
    $api_key_location = Url::fromUri('base:index_now_api_key_' . $api_key . '.txt');
    $api_key_location->setAbsolute();

    $url = Url::fromUri($endpoint);
    $url->setOption('query', [
      'url' => $page_url,
      'key' => $api_key,
      'keyLocation' => $api_key_location->toString(),
    ]);

    return $url;
  }

}
