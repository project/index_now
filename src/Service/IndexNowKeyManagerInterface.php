<?php

namespace Drupal\index_now\Service;

/**
 * Key generator service.
 */
interface IndexNowKeyManagerInterface {

  /**
   * Generates the Index Now API key.
   */
  public function generateKey() : void;

  /**
   * Get the Index Now API key.
   *
   * @return string
   *   The Index Now API key.
   */
  public function getKey() : string;

  /**
   * Get cache tags used by the key manager.
   *
   * @return array
   *   An array of cache tags.
   */
  public function getCacheTags() : array;

}
