<?php

namespace Drupal\index_now\Service;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Key generator service.
 */
final class IndexNowKeyManager implements IndexNowKeyManagerInterface {

  /**
   * The Index Now config.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Key Generator constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Component\Uuid\UuidInterface $uuid
   *   The uuid service.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected UuidInterface $uuid,
  ) {
    $this->config = $configFactory->getEditable('index_now.settings');
  }

  /**
   * {@inheritdoc}
   */
  public function generateKey(): void {
    // Generate a random API key.
    $api_key = $this->uuid->generate();

    // Store the API key.
    $this->config->set('api_key', $api_key)
      ->save();
  }

  /**
   * {@inheritdoc}
   */
  public function getKey() : string {
    return $this->config->get('api_key') ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags() : array {
    return [
      'config:index_now.settings',
    ];
  }

}
