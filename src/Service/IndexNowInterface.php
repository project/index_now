<?php

namespace Drupal\index_now\Service;

/**
 * Index now interface.
 */
interface IndexNowInterface {

  const ENDPOINTS = [
    'bing' => 'https://www.bing.com',
    'indexnow' => 'https://api.indexnow.org',
    'seznam' => 'https://search.seznam.cz',
    'yandex' => 'https://yandex.com',
    'naver' => 'https://searchadvisor.naver.com',
  ];

  /**
   * Send an index now request to a search engine.
   *
   * @param string $page_url
   *   The page we want to index now on the search engines.
   */
  public function sendRequest(string $page_url): void;

}
