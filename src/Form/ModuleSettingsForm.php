<?php

namespace Drupal\index_now\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\index_now\Service\IndexNowKeyManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Module settings form.
 */
class ModuleSettingsForm extends ConfigFormBase {

  /**
   * The Index Now key manager service.
   *
   * @var \Drupal\index_now\Service\IndexNowKeyManagerInterface
   */
  protected $indexNowKeyManager;

  /**
   * Tells if the node module is installed.
   *
   * @var bool
   */
  protected $isNodeEnabled;

  /**
   * Tells if the taxonomy module is installed.
   *
   * @var bool
   */
  protected $isTaxonomyEnabled;

  /**
   * The node types list.
   *
   * @var array
   */
  protected $nodeTypes;

  /**
   * The vocabularies list.
   *
   * @var array
   */
  protected $vocabularies;

  /**
   * The form construct.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typed_config_manager
   *   The typed config manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Drupal\index_now\Service\IndexNowKeyManagerInterface $index_now_key_manager
   *   The Index Now key manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typed_config_manager,
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandlerInterface $module_handler,
    IndexNowKeyManagerInterface $index_now_key_manager,
  ) {
    parent::__construct($config_factory, $typed_config_manager);
    $this->indexNowKeyManager = $index_now_key_manager;
    $this->isTaxonomyEnabled = $module_handler->moduleExists('taxonomy');
    $this->isNodeEnabled = $module_handler->moduleExists('node');
    $this->vocabularies = [];
    $this->nodeTypes = [];

    if ($this->isTaxonomyEnabled) {
      $this->vocabularies = $entity_type_manager
        ->getStorage('taxonomy_vocabulary')
        ->loadMultiple();
    }
    if ($this->isNodeEnabled) {
      $this->nodeTypes = $entity_type_manager
        ->getStorage('node_type')
        ->loadMultiple();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('index_now.apikey.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'index_now_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'index_now.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    $hooks_options = [
      'insert' => $this->t('Entity created'),
      'update' => $this->t('Entity updated'),
      'delete' => $this->t('Entity deleted'),
    ];

    // Check if there's an API key.
    $api_key = $this->indexNowKeyManager->getKey();

    if (empty($api_key)) {
      // We don't use service messenger because the warning remains
      // after the key has been generated until we reload the page.
      $form['no_keyfile'] = [
        '#markup' => "<p>⚠️ " . $this->t("Index now API key has not been generated yet. You must generate it if you want to use Index Now on this environment.") . "</p>",
      ];

      $form['generate_keyfile'] = [
        '#type' => 'button',
        '#value' => $this->t("Generate the API key"),
        '#name' => 'generate_apikey',
        '#attributes' => [
          'class' => [
            'button',
            'button-primary',
          ],
        ],
      ];
    }

    if ($this->isNodeEnabled || $this->isTaxonomyEnabled) {
      $form['tabs'] = [
        '#type' => 'vertical_tabs',
        '#title' => $this->t("Restrict use"),
      ];
    }

    if ($this->isNodeEnabled) {
      $form['node'] = [
        '#type' => 'details',
        '#title' => $this->t("Content types"),
        '#group' => 'tabs',
      ];

      $node_types = [];
      /** @var \Drupal\node\Entity\NodeType $node_type */
      foreach ($this->nodeTypes as $machine_name => $node_type) {
        $node_types[$machine_name] = $node_type->get('name');
      }

      $form['node']['exclude_node_types'] = [
        '#type' => 'checkboxes',
        '#options' => $node_types,
        '#title' => $this->t("Exclude node types"),
        '#description' => $this->t("Selected node types won't use Index now"),
        '#default_value' => $this->config('index_now.settings')
          ->get('exclude_node_types') ?? [],
      ];

      $form['node']['exclude_node_events'] = [
        '#type' => 'checkboxes',
        '#options' => $hooks_options,
        '#title' => $this->t("Exclude events"),
        '#description' => $this->t("Selected events won't use Index now"),
        '#default_value' => $this->config('index_now.settings')
          ->get('exclude_node_events') ?? [],
      ];
    }

    if ($this->isTaxonomyEnabled) {
      $form['taxonomy'] = [
        '#type' => 'details',
        '#title' => $this->t("Taxonomies"),
        '#group' => 'tabs',
      ];

      $vocabularies = [];
      /** @var \Drupal\taxonomy\Entity\Vocabulary $vocabulary */
      foreach ($this->vocabularies as $machine_name => $vocabulary) {
        $vocabularies[$machine_name] = $vocabulary->get('name');
      }

      $form['taxonomy']['exclude_vocabularies'] = [
        '#type' => 'checkboxes',
        '#options' => $vocabularies,
        '#title' => $this->t("Exclude vocabularies"),
        '#description' => $this->t("Selected vocabularies won't use Index now"),
        '#default_value' => $this->config('index_now.settings')
          ->get('exclude_vocabularies') ?? [],
      ];

      $form['taxonomy']['exclude_taxonomy_events'] = [
        '#type' => 'checkboxes',
        '#options' => $hooks_options,
        '#title' => $this->t("Exclude events"),
        '#description' => $this->t("Selected events won't use Index now"),
        '#default_value' => $this->config('index_now.settings')
          ->get('exclude_taxonomy_events') ?? [],
      ];
    }

    $form['default_engine'] = [
      '#type' => 'radios',
      '#options' => [
        'bing' => $this->t('Microsoft Bing'),
        'indexnow' => $this->t('IndexNow'),
        'seznam' => $this->t('Seznam.cz'),
        'yandex' => $this->t('Yandex'),
        'naver' => $this->t('Naver'),
      ],
      '#title' => $this->t("Default search engine to interact with"),
      '#description' => $this->t("Since november 2021, if you interact with one search engine, you interact with all."),
      '#default_value' => $this->config('index_now.settings')
        ->get('default_engine') ?? 'bing',
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $triggering_element = $form_state->getTriggeringElement();
    if ($triggering_element['#name'] == 'generate_apikey') {
      $this->indexNowKeyManager->generateKey();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('index_now.settings');
    $config->set('default_engine', $form_state->getValue('default_engine'));
    if ($this->isNodeEnabled) {
      $config->set('exclude_node_types', $form_state->getValue('exclude_node_types'));
      $config->set('exclude_node_events', $form_state->getValue('exclude_node_events'));
    }
    if ($this->isTaxonomyEnabled) {
      $config->set('exclude_vocabularies', $form_state->getValue('exclude_vocabularies'));
      $config->set('exclude_taxonomy_events', $form_state->getValue('exclude_taxonomy_events'));
    }
    $config->save();

    parent::submitForm($form, $form_state);
  }

}
