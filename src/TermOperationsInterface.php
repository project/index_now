<?php

namespace Drupal\index_now;

use Drupal\taxonomy\Entity\Term;

/**
 * Node operations interface.
 */
interface TermOperationsInterface {

  /**
   * Acts on a term operation for the allowed vocabularies.
   *
   * @param \Drupal\taxonomy\Entity\Term $term
   *   The term being updated / deleted.
   * @param string $event
   *   An 'insert', 'update' or 'delete' event.
   */
  public function pingIndexNow(Term $term, string $event): void;

  /**
   * Tells if a term is indexable or not.
   *
   * @param \Drupal\taxonomy\Entity\Term $term
   *   The term being updated / deleted.
   * @param string $event
   *   An 'insert', 'update' or 'delete' event.
   *
   * @return bool
   *   Return true is the vocabulary has not been excluded in the Index Now conf
   *   if the term is published and if the anonymous role has "access content"
   *   permission.
   */
  public function isIndexable(Term $term, string $event): bool;

}
