<?php

namespace Drupal\index_now\Drush\Commands;

use Drupal\Component\DependencyInjection\ContainerInterface;
use Drupal\index_now\Service\IndexNowKeyManagerInterface;
use Drush\Commands\DrushCommands;

/**
 * Drush index_now key commands.
 */
class IndexNowKeyCommands extends DrushCommands {

  /**
   * Class constructor.
   *
   * @param \Drupal\index_now\Service\IndexNowKeyManagerInterface $indexNowKeyManager
   *   The Index Now key manager.
   */
  public function __construct(protected IndexNowKeyManagerInterface $indexNowKeyManager) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('index_now.apikey.manager')
    );
  }

  /**
   * Generate the key.
   *
   * @command index_now:keygenerate
   * @aliases indnowkeygen
   *
   * @usage drush index_now:keygenerate
   */
  public function generateKey() {
    $this->indexNowKeyManager->generateKey();
    $key = $this->indexNowKeyManager->getKey();
    $this->output()->writeln('Index Now API key as been generated: ' . $key);
  }

}
