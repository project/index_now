<?php

namespace Drupal\index_now;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\index_now\Service\IndexNowInterface;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Node operations class.
 */
class TermOperations implements ContainerInjectionInterface, TermOperationsInterface {

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorage
   */
  protected $userStorage;

  /**
   * Node operations class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\index_now\Service\IndexNowInterface $indexNow
   *   The index now service.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected IndexNowInterface $indexNow,
  ) {
    $this->userStorage = $entityTypeManager->getStorage('user');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('index_now.indexnow')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function pingIndexNow(Term $term, string $event): void {
    if ($this->isIndexable($term, $event)) {
      $options = [
        'absolute' => TRUE,
        'language' => $term->language(),
      ];
      $term_url = Url::fromRoute('entity.taxonomy_term.canonical', ['taxonomy_term' => $term->id()], $options);
      $this->indexNow->sendRequest($term_url->toString());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isIndexable(Term $term, string $event): bool {
    $anonymous_user = $this->userStorage->load(0);
    if (!$term->access('view', $anonymous_user)) {
      return FALSE;
    }

    $exclude_events = $this->configFactory
      ->getEditable('index_now.settings')
      ->get('exclude_taxonomy_events');

    if (isset($exclude_events[$event]) && !empty($exclude_events[$event])) {
      return FALSE;
    }

    $exclude_vocabularies = $this->configFactory
      ->getEditable('index_now.settings')
      ->get('exclude_vocabularies');
    $exclude_vocabularies = !is_array($exclude_vocabularies) ? [] : array_filter(array_values($exclude_vocabularies));

    return !in_array($term->bundle(), $exclude_vocabularies);
  }

}
