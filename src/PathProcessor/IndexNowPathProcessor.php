<?php

namespace Drupal\index_now\PathProcessor;

use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Drupal\index_now\Service\IndexNowKeyManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Index Now path processor.
 */
class IndexNowPathProcessor implements InboundPathProcessorInterface {

  /**
   * Class constructor.
   *
   * @param \Drupal\index_now\Service\IndexNowKeyManagerInterface $indexNowKeyManager
   *   The Index Now key manager.
   */
  public function __construct(protected IndexNowKeyManagerInterface $indexNowKeyManager) {
  }

  /**
   * {@inheritdoc}
   */
  public function processInbound($path, Request $request) {
    $api_key = $this->indexNowKeyManager->getKey();
    if (!empty($api_key) && '/index_now_api_key_' . $api_key . '.txt' === $path) {
      return '/index_now_api_key/' . $api_key;
    }

    return $path;
  }

}
