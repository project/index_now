<?php

namespace Drupal\index_now\Hook;

use Drupal\node\Entity\Node;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Hook\Attribute\Hook;
use Drupal\index_now\NodeOperations;
use Drupal\index_now\PathAliasOperations;
use Drupal\index_now\TermOperations;
use Drupal\path_alias\Entity\PathAlias;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Entity actions hooks.
 */
class EntityActions implements ContainerInjectionInterface {

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\DependencyInjection\ClassResolverInterface $classResolver
   *   The class resolver.
   */
  public function __construct(protected ClassResolverInterface $classResolver) {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): static {
    return new static(
      $container->get('class_resolver'),
    );
  }

  /**
   * Implements hook_ENTITY_TYPE_insert() for path alias.
   */
  #[Hook('path_alias_insert')]
  public function pathAliasInsert(PathAlias $path_alias): void {
    $this->classResolver
      ->getInstanceFromDefinition(PathAliasOperations::class)
      ->pingIndexNow($path_alias);
  }

  /**
   * Implements hook_ENTITY_TYPE_update() for node.
   */
  #[Hook('node_update')]
  public function nodeUpdate(Node $node): void {
    $this->classResolver
      ->getInstanceFromDefinition(NodeOperations::class)
      ->pingIndexNow($node, 'update');
  }

  /**
   * Implements hook_ENTITY_TYPE_delete() for node.
   */
  #[Hook('node_delete')]
  public function nodeDelete(Node $node): void {
    $this->classResolver
      ->getInstanceFromDefinition(NodeOperations::class)
      ->pingIndexNow($node, 'delete');
  }

  /**
   * Implements hook_ENTITY_TYPE_update() for taxonomy term.
   */
  #[Hook('taxonomy_term_update')]
  public function taxonomyTermUpdate(Term $term): void {
    $this->classResolver
      ->getInstanceFromDefinition(TermOperations::class)
      ->pingIndexNow($term, 'update');
  }

  /**
   * Implements hook_ENTITY_TYPE_delete() for taxonomy term.
   */
  #[Hook('taxonomy_term_delete')]
  public function taxonomyTermDelete(Term $term): void {
    $this->classResolver
      ->getInstanceFromDefinition(TermOperations::class)
      ->pingIndexNow($term, 'delete');
  }

}
