<?php

namespace Drupal\index_now\Hook;

use Drupal\Core\Hook\Attribute\Hook;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

/**
 * Help hooks implementation.
 */
final class Help {

  use StringTranslationTrait;

  /**
   * Implements hook_help().
   */
  #[Hook('help')]
  public function __invoke(string $route_name, RouteMatchInterface $route_match) {
    switch ($route_name) {
      case 'help.page.index_now':
        // Introduction.
        $help = '<h2>' . $this->t("Introduction") . '</h2>';
        $help .= '<p>' . $this->t("<strong>Index Now</strong> submit requests to search engines when content has been created, updated or deleted on your website.") . '</p>';
        $help .= '<p>' . $this->t("Supported search engines are:") . '<br/>';
        $help .= '<ul>';
        $help .= '<li>Microsoft Bing</li>';
        $help .= '<li>Seznam.cz</li>';
        $help .= '<li>Yandex</li>';
        $help .= '<li>Naver</li>';
        $help .= '</ul>';
        $help .= '<p>';
        $help .= $this->t("Note: Since November 2021, compatibles search engines will immediately share all the submitted URLs with every other compatible search engine.") . '<br />';
        $help .= $this->t("This means that if you choose to notify one search engine, you notify every other one in the same time.");
        $help .= '</p>';
        $help .= $this->t('<a href=":protocol_url">Read more about Index Now project</a>', [
          ':protocol_url' => 'https://www.indexnow.org',
        ]);
        // Requirements.
        $help .= '<h2>' . $this->t("Requirements") . '</h2>';
        $help .= '<p>' . $this->t("This module requires no modules outside of Drupal core.") . '</p>';
        // Installation.
        $help .= '<h2>' . $this->t("Installation") . '</h2>';
        $help .= '<p>';
        $help .= $this->t('Install as you would normally install a contributed Drupal module. Visit <a href=":installation_help">:installation_help</a> for further information.', [
          ':installation_help' => 'https://www.drupal.org/node/1897420',
        ]);
        $help .= '</p>';
        // Configuration.
        $help .= '<h2>' . $this->t("Configuration") . '</h2>';
        $help .= '<p>';
        $help .= $this->t('Go to <a href=":config_page">:config_page</a>, generate the API key and choose the default search engine you want to interact with.', [
          ':config_page' => Url::fromRoute('index_now.settings')->toString(),
        ]) . '<br />';
        $help .= $this->t("You can also exclude content types, taxonomy vocabularies, commerce products types for which you don't want to use Index Now.") . '<br/>';
        $help .= $this->t("If you don't want to use that feature on an environment (ex: local / preprod) just don't generate the API key.");
        $help .= '</p>';
        // Service.
        $help .= '<h2>' . $this->t("Service") . '</h2>';
        $help .= '<p>' . $this->t("The module provides a service that can be used to index other URLs than just nodes, taxonomy terms and commerce products pages.") . '</p>';
        $help .= '<p>';
        $help .= $this->t("Here is how to use it:") . '<br/>';
        $help .= '<code>' . "\Drupal::service('index_now.indexnow')->sendRequest(YOUR_URL);" . '</code>';
        $help .= '</p>';
        $help .= '<p><strong>' . $this->t("Note:") . '</strong> ' . $this->t("YOUR_URL must be an absolute URL hosted on your website.") . '</br>';
        $help .= $this->t("Also keep in mind that the page needs to be accessible by an anonymous user.");
        $help .= '</p>';

        return $help;
    }
  }

}
