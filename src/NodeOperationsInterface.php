<?php

namespace Drupal\index_now;

use Drupal\node\Entity\Node;

/**
 * Node operations interface.
 */
interface NodeOperationsInterface {

  /**
   * Acts on a node operation (insert/update/delete) for the allowed node types.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node being inserted / updated / deleted.
   * @param string $event
   *   An 'insert', 'update' or 'delete' event.
   */
  public function pingIndexNow(Node $node, string $event): void;

  /**
   * Tells if a node is indexable or not.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The node being inserted / updated / deleted.
   * @param string $event
   *   An 'insert', 'update' or 'delete' event.
   *
   * @return bool
   *   Return true is the node type has not been excluded in the Index Now conf,
   *   if the node is published and if the anonymous role has "access content"
   *   permission.
   */
  public function isIndexable(Node $node, string $event): bool;

}
