<?php

namespace Drupal\index_now;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Url;
use Drupal\index_now\Service\IndexNowInterface;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Node operations class.
 */
class NodeOperations implements ContainerInjectionInterface, NodeOperationsInterface {

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorage
   */
  protected $userStorage;

  /**
   * Node operations class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The config factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\index_now\Service\IndexNowInterface $indexNow
   *   The index now service.
   */
  public function __construct(
    protected ConfigFactoryInterface $configFactory,
    protected EntityTypeManagerInterface $entityTypeManager,
    protected IndexNowInterface $indexNow,
  ) {
    $this->userStorage = $entityTypeManager->getStorage('user');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('index_now.indexnow')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function pingIndexNow(Node $node, string $event): void {
    if ($this->isIndexable($node, $event)) {
      $options = [
        'absolute' => TRUE,
        'language' => $node->language(),
      ];
      $node_url = Url::fromRoute('entity.node.canonical', ['node' => $node->id()], $options);
      $this->indexNow->sendRequest($node_url->toString());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function isIndexable(Node $node, string $event): bool {
    $anonymous_user = $this->userStorage->load(0);
    if (!$node->access('view', $anonymous_user)) {
      return FALSE;
    }

    $exclude_events = $this->configFactory
      ->getEditable('index_now.settings')
      ->get('exclude_node_events');

    if (isset($exclude_events[$event]) && !empty($exclude_events[$event])) {
      return FALSE;
    }

    $exclude_node_types = $this->configFactory
      ->getEditable('index_now.settings')
      ->get('exclude_node_types');
    $exclude_node_types = !is_array($exclude_node_types) ? [] : array_filter(array_values($exclude_node_types));

    return !in_array($node->getType(), $exclude_node_types);
  }

}
