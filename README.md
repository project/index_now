# Index Now


CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

**Index Now** submit requests to search engines when content (nodes /
taxonomy terms) has been created, updated or deleted on your website.
The module provides a service that you can use to send more requests
according to your needs.

Supported search engines are:
- Microsoft Bing
- Yandex
- Seznam.cz
- Naver
Note: Since November 2021, compatibles search engines will immediately share
all the submitted URLs with every other compatible search engine.
This means that if you choose to notify one search engine,
you notify every other one in the same time.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/index_now

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/index_now


REQUIREMENTS
------------

This module requires [Config split](https://www.drupal.org/project/config_split)
module in order to activate it only on a production environment.
Commerce products management has been removed from the main module, you will
need to install [Index Now Commerce](https://www.drupal.org/project/index_now_commerce)
if your project has commerce products in it


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

Go to `/admin/config/services/index_now` and choose the default search engine
you want to interact with.
You can also exclude content types, taxonomy vocabularies and/or commerce
products types for which you don't want to use Index Now.
An API key has been generated for you when you installed/updated the module.

This module has to be used ONLY on a production environment (search engines
can't crawl your local machine and there is no reason to index a preprod env).

In order to use this module only on a production environment, you will have to
define a dedicated config for your production environment using the module
config_split (`/admin/config/development/configuration/config-split`) and
include Index Now in the "Complete split" section.

SERVICE
-------

The module provides a service that can be used to index other URLs than just
nodes and taxonomy terms pages.

Here is how to use it:
`\Drupal::service('index_now.indexnow')->sendRequest($your_url);`

**Note:** `$your_url` must be an absolute URL hosted on your website.
Also keep in mind that the page needs to be accessible by an anonymous user.


DRUSH COMMAND
-------------

In case you would need to regenerate the API key you can use the drush command:
`drush index_now:keygenerate` or its alias `drush indnowkeygen`


MAINTAINERS
-----------

Current maintainers:
 * Maxime Roux (MacSim) - https://www.drupal.org/u/macsim
